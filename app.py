# web-app for API image manipulation

from flask import Flask, request, render_template, send_from_directory
import os
import torch
import numpy as np
from PIL import Image
# from br_inference import inference
from br_inference_rembg import inference
# from br_inference_u2net import inference
from br_models import *
# from br_models_u2net.model import *
import cv2
import urllib.request
import json
import base64
import io

# define ISNET
"""isnet=ISNetDIS()
model_path="saved_models/isnet.pth"  # the model path
if torch.cuda.is_available():
    isnet.load_state_dict(torch.load(model_path))
    net=isnet.cuda()
else:
    isnet.load_state_dict(torch.load(model_path,map_location="cpu"))
net.eval()
"""
### Define U2Net
# --------- 1. get image path and name ---------
"""model_name='u2net'#u2netp

# --------- 3. model define ---------
if(model_name=='u2net'):
    print("...load U2NET---173.6 MB")
    net = U2NET(3,1)
elif(model_name=='u2netp'):
    print("...load U2NEP---4.7 MB")
    net = U2NETP(3,1)
model_dir = os.path.join(os.getcwd(), 'saved_models', model_name + '.pth')

if torch.cuda.is_available():
    net.load_state_dict(torch.load(model_dir))
    net.cuda()
else:
    net.load_state_dict(torch.load(model_dir, map_location='cpu'))
net.eval()"""
##############

"""
define rembg
"""
from transparent_background import Remover
net = Remover()

app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


# default access page
@app.route("/")
def main():
    return render_template('index.html')


# upload selected image and forward to processing page
@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT, 'static/images/')

    # create image directory if not found
    if not os.path.isdir(target):
        os.mkdir(target)

    # retrieve file from html file-picker
    upload = request.files.getlist("file")[0]
    print("File name: {}".format(upload.filename))
    filename = upload.filename

    # file support verification
    ext = os.path.splitext(filename)[1]
    if (ext == ".jpg") or (ext == ".png") or (ext == ".bmp"):
        print("File accepted")
    else:
        return render_template("error.html", message="The selected file is not supported"), 400

    # save file
    destination = "/".join([target, filename])
    print("File saved to to:", destination)
    upload.save(destination)

    # forward to processing page
    return render_template("processing.html", image_name=filename)


# rotate filename the specified degrees
@app.route("/rotate", methods=["POST"])
def rotate():
    # retrieve parameters from html form
    angle = request.form['angle']
    filename = request.form['image']

    # open and process image
    target = os.path.join(APP_ROOT, 'static/images')
    destination = "/".join([target, filename])

    img = Image.open(destination)
    img = img.rotate(-1*int(angle))

    # save and return image
    destination = "/".join([target, 'temp.png'])
    if os.path.isfile(destination):
        os.remove(destination)
    img.save(destination)

    return send_image('temp.png')


# flip filename 'vertical' or 'horizontal'
@app.route("/flip", methods=["POST"])
def flip():

    # retrieve parameters from html form
    if 'horizontal' in request.form['mode']:
        mode = 'horizontal'
    elif 'vertical' in request.form['mode']:
        mode = 'vertical'
    else:
        return render_template("error.html", message="Mode not supported (vertical - horizontal)"), 400
    filename = request.form['image']

    # open and process image
    target = os.path.join(APP_ROOT, 'static/images')
    destination = "/".join([target, filename])

    img = Image.open(destination)

    if mode == 'horizontal':
        img = img.transpose(Image.FLIP_LEFT_RIGHT)
    else:
        img = img.transpose(Image.FLIP_TOP_BOTTOM)

    # save and return image
    destination = "/".join([target, 'temp.png'])
    if os.path.isfile(destination):
        os.remove(destination)
    img.save(destination)

    return send_image('temp.png')


# crop filename from (x1,y1) to (x2,y2)
@app.route("/crop", methods=["POST"])
def crop():
    # retrieve parameters from html form
    x1 = int(request.form['x1'])
    y1 = int(request.form['y1'])
    x2 = int(request.form['x2'])
    y2 = int(request.form['y2'])
    filename = request.form['image']

    # open image
    target = os.path.join(APP_ROOT, 'static/images')
    destination = "/".join([target, filename])

    img = Image.open(destination)

    # check for valid crop parameters
    width = img.size[0]
    height = img.size[1]

    crop_possible = True
    if not 0 <= x1 < width:
        crop_possible = False
    if not 0 < x2 <= width:
        crop_possible = False
    if not 0 <= y1 < height:
        crop_possible = False
    if not 0 < y2 <= height:
        crop_possible = False
    if not x1 < x2:
        crop_possible = False
    if not y1 < y2:
        crop_possible = False

    # crop image and show
    if crop_possible:
        img = img.crop((x1, y1, x2, y2))
        
        # save and return image
        destination = "/".join([target, 'temp.png'])
        if os.path.isfile(destination):
            os.remove(destination)
        img.save(destination)
        return send_image('temp.png')
    else:
        return render_template("error.html", message="Crop dimensions not valid"), 400
    return '', 204


# blend filename with stock photo and alpha parameter
@app.route("/blend", methods=["POST"])
def blend():
    # retrieve parameters from html form
    alpha = request.form['alpha']
    filename1 = request.form['image']

    # open images
    target = os.path.join(APP_ROOT, 'static/images')
    filename2 = 'blend.jpg'
    destination1 = "/".join([target, filename1])
    destination2 = "/".join([target, filename2])

    img1 = Image.open(destination1)
    img2 = Image.open(destination2)

    # resize images to max dimensions
    width = max(img1.size[0], img2.size[0])
    height = max(img1.size[1], img2.size[1])

    img1 = img1.resize((width, height), Image.ANTIALIAS)
    img2 = img2.resize((width, height), Image.ANTIALIAS)

    # if image in gray scale, convert stock image to monochrome
    if len(img1.mode) < 3:
        img2 = img2.convert('L')

    # blend and show image
    img = Image.blend(img1, img2, float(alpha)/100)

     # save and return image
    destination = "/".join([target, 'temp.png'])
    if os.path.isfile(destination):
        os.remove(destination)
    img.save(destination)

    return send_image('temp.png')

# remove br filename the specified degrees
@app.route("/br", methods=["POST"])
def br():
    # retrieve parameters from html form
    filename = request.form['image']

    # open and process image
    target = os.path.join(APP_ROOT, 'static/images')
    destination = "/".join([target, filename])

    img = Image.open(destination)
    # img = np.array(img)
    img = inference(net, img)
    img = np.array(img)
    # font = cv2.FONT_HERSHEY_SIMPLEX
    # cv2.putText(img, 'For Demo Purpose', (10,450), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
    img = Image.fromarray(img.astype(np.uint8))

    # save and return image
    destination = "/".join([target, 'temp.png'])
    if os.path.isfile(destination):
        os.remove(destination)
    img.save(destination)

    return send_image('temp.png')

# rotate filename the specified degrees
# @app.route("/outpaint", methods=["POST"])
# def outpaint():
#     # retrieve parameters from html form
#     filename = request.form['image']
#     border = int(request.form['pixels'])

#     # open and process image
#     target = os.path.join(APP_ROOT, 'static/images')
#     destination = "/".join([target, filename])

#     img = Image.open(destination)
#     img = np.array(img)
#     num = border // 2
#     rem = border % 2
#     for _ in range(num):
#         img = extend_border_smoother(img, 5)
#     img = extend_border_smoother(img, rem)
#     font = cv2.FONT_HERSHEY_SIMPLEX
#     cv2.putText(img, 'For Demo Purpose', (10,450), font, 3, (0, 255, 0), 2, cv2.LINE_AA)
#     img = Image.fromarray(img.astype(np.uint8))

#     # save and return image
#     destination = "/".join([target, 'temp.png'])
#     if os.path.isfile(destination):
#         os.remove(destination)
#     img.save(destination)

#     return send_image('temp.png')

API_URL = "http://127.0.0.1:7861"

@app.route("/outpaint", methods=["POST"])
def outpaint():
    # retrieve parameters from html form
    filename = request.form['image']
    border = int(request.form['pixels'])
    strength = float(request.form['strength']) / 100
    # open and process image
    target = os.path.join(APP_ROOT, 'static/images')
    destination = "/".join([target, filename])
    img_orig = Image.open(destination)
    w0, h0 = img_orig.size
    print(w0, h0)
    w0 += border * 2
    h0 += border * 2

    init_images = [
        encode_file_to_base64(destination),
        # encode_file_to_base64(r"B:\path\to\img_2.png"),
        # "https://image.can/also/be/a/http/url.png",
    ]

    batch_size = 1
    payload = {
        "prompt": "",
        "resize_mode": 0,
        # "mask": init_images,
        "seed": 1,
        "steps": 20,
        "width": 512,
        "height": 512,
        "denoising_strength": strength,
        "n_iter": 1,
        "init_images": init_images,
        "sampler_index": "Euler",
        "script_name": "Poor man's outpainting",
        "script_args":[border, 4, 'fill', ['left', 'right', 'up', 'down']],
        "batch_size": batch_size if len(init_images) == 1 else len(init_images),
        # "mask": init_images,
    }
    
    img = call_img2img_api(**payload)
    img = Image.open(io.BytesIO(img))

    # img = Image.open(destination)
    # img = np.array(img)
    w, h = img.size

    w_to_cut = int((w - w0)/2)
    h_to_cut = int((h - h0)/2)

    print(h, w, h0, w0, h_to_cut, w_to_cut)


    # save and return image
    destination = "/".join([target, 'temp.png'])
    if os.path.isfile(destination):
        os.remove(destination)
    img_np = np.array(img)
    if h_to_cut == 0 and w_to_cut !=0:
        img_np = img_np[ :, w_to_cut:-w_to_cut, ...]
    if w_to_cut == 0 and h_to_cut != 0:
        img_np = img_np[ h_to_cut:-h_to_cut, :, ...]
    if w_to_cut !=0 and h_to_cut !=0:
        img_np = img_np[ h_to_cut:-h_to_cut, w_to_cut:-w_to_cut, ...]

    print(img_np.shape)
    img = Image.fromarray(img_np)
    img.save(destination)
    
    return send_image('temp.png')

def extend_border_smooth(image, border_size=10):
    # retrieve parameters from html form
    filename = request.form['image']

    # Get the original image dimensions
    height, width = image.shape[:2]

    # Create a new image with extended borders
    extended_image = np.zeros((height + 2 * border_size, width + 2 * border_size, 3), dtype=np.uint8)

    # Copy the original image into the center of the new image
    extended_image[border_size:height + border_size, border_size:width + border_size] = image

    # Mirror padding for smooth extension
    extended_image[:border_size, border_size:-border_size] = np.flipud(image[:border_size, :])
    extended_image[-border_size:, border_size:-border_size] = np.flipud(image[-border_size:, :])
    extended_image[border_size:-border_size, :border_size] = np.fliplr(image[:, :border_size])
    extended_image[border_size:-border_size, -border_size:] = np.fliplr(image[:, -border_size:])

    return extended_image

def extend_border_smoother(image, border_size=10):
    # Use cv2.copyMakeBorder with BORDER_REFLECT for smooth extension
    extended_image = cv2.copyMakeBorder(image, border_size, border_size, border_size, border_size, cv2.BORDER_REFLECT)

    return extended_image

def call_api(api_endpoint, **payload):
    data = json.dumps(payload).encode('utf-8')
    request = urllib.request.Request(
        f'{API_URL}/{api_endpoint}',
        headers={'Content-Type': 'application/json'},
        data=data,
    )
    response = urllib.request.urlopen(request)
    return json.loads(response.read().decode('utf-8'))

def encode_file_to_base64(path):
    with open(path, 'rb') as file:
        return base64.b64encode(file.read()).decode('utf-8')

def call_img2img_api(**payload):
    response = call_api('sdapi/v1/img2img', **payload)
    image = response.get('images')[0]
    image = decode_and_return(image)
    return image

def decode_and_return(base64_str):
    return base64.b64decode(base64_str)

# retrieve file from 'static/images' directory
@app.route('/static/images/<filename>')
def send_image(filename):
    return send_from_directory("static/images", filename)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001)